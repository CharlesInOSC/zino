//! Re-exports of common types in [`zino-core`].
//!
//! [`zino-core`]: https://docs.rs/zino-core

#[doc(no_inline)]
pub use zino_core::{
    application::Application,
    auth::{
        AccessKeyId, AuthorizationProvider, JwtClaims, SecretAccessKey, SecurityToken, UserSession,
    },
    datetime::DateTime,
    error::Error,
    extension::{JsonObjectExt, JsonValueExt, TomlTableExt},
    file::NamedFile,
    fluent_args, json,
    model::{Model, ModelHooks, Mutation, Query, QueryContext},
    reject,
    request::{RequestContext, Validation},
    response::{ExtractRejection, Rejection, StatusCode, WebHook},
    schedule::{AsyncCronJob, CronJob},
    state::State,
    BoxFuture, Map, Record, Uuid,
};

#[cfg(feature = "orm")]
#[doc(no_inline)]
pub use zino_core::orm::{ModelAccessor, ModelHelper, Schema};
